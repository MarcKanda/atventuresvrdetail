﻿Shader "Kanda/Highlight"
{
    Properties
	{
		_Color("Color", Color) = (0.0, 0.65, 0.95, 1.0)
		_MainTex ("Falloff (R)", 2D) = "white" {}
		_Opacity("Opacity", Range(0, 10.0)) = 2.0
    }
    SubShader
	{
        Tags
		{
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
		}

        LOD 200

		Pass
		{
			ZWrite On
			ColorMask 0
		}

		Lighting Off
		Offset -1, -1

        CGPROGRAM
        #pragma surface surf NoLighting alpha noforwardadd noshadow noambient novertexlights nolightmap nodynlightmap nodirlightmap nofog nometa nolppv noshadowmask 
       
		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
		{
			fixed4 c;
			c.rgb = s.Albedo; 
			c.a = s.Alpha;
			return c;
		}
 
		struct Input
		{
			float4 color: COLOR;
			float3 viewDir;
		};

		fixed4 _Color;
		fixed _Opacity;
		sampler2D _MainTex;
 
		void surf (Input IN, inout SurfaceOutput o)
		{
			o.Emission = _Color.rgb;

			half NdotE = saturate(dot(o.Normal, IN.viewDir));
			half4 falloff = tex2D(_MainTex, half2(NdotE, NdotE));
			o.Alpha = _Color.a * IN.color.a * falloff.r * _Opacity;

			/*
			//o.Albedo = _Color.rgb;
			o.Alpha = _Color.a * IN.color.a * pow(1.0 - saturate(dot(IN.viewDir, o.Normal)), _Fresnel) * _Opacity;
			o.Emission = _Color.rgb;
			*/
		}
		ENDCG
    }
    FallBack "Diffuse"
}