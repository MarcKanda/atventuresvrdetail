﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions {

	public static void DestroyAllChildren(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public static void SetColliders(this Transform transform, bool toState)
    {
        Collider[] colliders = transform.gameObject.GetComponentsInChildren<Collider>();

        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].enabled = toState;
        }
    }
}
