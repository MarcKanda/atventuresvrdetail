﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryItem", menuName = "Inventory/Item")]
public class InventoryItem : ScriptableObject {

    public string itemName;
    public Sprite itemThumbnail;
    public GameObject itemPrefab;
}
