﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryGroup", menuName = "Inventory/Group")]
public class InventoryGroup : ScriptableObject {

    public string groupName;
    public Sprite groupThumbnail;
    public InventoryItem[] items;
    public bool disableGroup;
}
