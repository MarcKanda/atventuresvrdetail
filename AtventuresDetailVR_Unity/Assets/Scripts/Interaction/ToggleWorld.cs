﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ToggleWorld : MonoBehaviour {

    public VRTK_ControllerEvents controllerEvents;
    public Transform playerTransform;
    public Transform upScaleTransform;
    public Transform downScaleTransform;
    public Transform targetTransform;
    public GameObject derpartmentParent;
    public VRTK_InteractObjectHighlighter highLighterOne;
    public VRTK_InteractObjectHighlighter higherLighterTwo;

    private bool isCurrentlyUpScaled = false;

    [SerializeField] Transform table;
    Vector3 initialDepartmentPosition;
    public void Awake()
    {
        initialDepartmentPosition = derpartmentParent.transform.localPosition;
        SetScale(false);
    }

    public void OnEnable()
    {
        controllerEvents.GripReleased += OnGrabPressed;
    }

    public void OnDisable()
    {
        controllerEvents.GripReleased -= OnGrabPressed;
    }

    //Sets the scale of the department store and disables the department selections
    public void SetScale(bool isUpscaled)
    {
        targetTransform.SetParent(isUpscaled ? upScaleTransform : downScaleTransform);
        targetTransform.localScale = Vector3.one;
        targetTransform.localPosition = Vector3.zero;
        derpartmentParent.transform.localPosition = !isUpscaled ? initialDepartmentPosition : Vector3.down * 1000f;
        isCurrentlyUpScaled = isUpscaled;
    }

    private void OnGrabPressed(object sender, ControllerInteractionEventArgs args)
    {
        ReturnPlayerToPosition();
    }

    //Returns the player to a position outside the miniature store
    public void ReturnPlayerToPosition()
    {
        if (isCurrentlyUpScaled)
        {
            Vector3 toPlayerPosition = table.position + Vector3.ClampMagnitude(table.position - playerTransform.position, 0.5f);
            playerTransform.position = toPlayerPosition;

            SetScale(false);
        }
    }

    public void PutPlayerInStore(Transform department)
    {
        SetScale(true);
        Vector3 playerPosition = department.position;
        playerPosition.y = 0;
        Debug.Log(department.name);
        playerTransform.position = playerPosition;
    }
}
