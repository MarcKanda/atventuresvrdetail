﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class InteractableDepartment : MonoBehaviour
{
    [SerializeField]
    ToggleWorld worldToggler;

    [SerializeField]
    VRTK_InteractableObject interactableObject; 

    public void OnDisable()
    {
        interactableObject.InteractableObjectUnused -= OnDepartmentDeselected;
    }

    public void OnEnable()
    {
        if (interactableObject == null) interactableObject = GetComponent<VRTK_InteractableObject>();
        
        interactableObject.InteractableObjectUnused += OnDepartmentDeselected;
    }

    private void OnDepartmentDeselected(object sender, InteractableObjectEventArgs e)
    {

        worldToggler.PutPlayerInStore(transform);
    }
}