﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using UnityEngine.EventSystems;


public class InteractionParser : MonoBehaviour {

    public VRTK_Pointer pointer;

    public void Awake()
    {
        pointer.SelectionButtonPressed += OnSelectionButtonPressed;
        pointer.SelectionButtonReleased += OnSelectionButtonReleased;
    }

    private void OnSelectionButtonPressed(object sender, ControllerInteractionEventArgs args)
    {
        Debug.Log("WUUP");
    }

    private void OnSelectionButtonReleased(object sender, ControllerInteractionEventArgs args)
    {
        if(sender != null)
        {
            IPointerUpHandler pointerUpHandler = GetComponent<IPointerUpHandler>();
            if(pointerUpHandler != null)
            {
                
            }
        }
        
    }
}
