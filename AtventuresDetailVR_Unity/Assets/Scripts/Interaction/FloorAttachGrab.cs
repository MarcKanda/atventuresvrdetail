﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.GrabAttachMechanics;

public class FloorAttachGrab : VRTK_BaseGrabAttach
{
    public LayerMask layerMask;

    public override void ProcessUpdate()
    {
        base.ProcessUpdate();

        PlaceObject(controllerAttachPoint.position);
    }

    private void PlaceObject(Vector3 attachPointPosition)
    {
        //By default the object is placed at the handle
        transform.position = attachPointPosition;

        //We force the object to always point upwards
        transform.up = Vector3.up;

        //We check to see if we can find some ground the object can be placed on
        RaycastHit rayCastHit;
        Ray ray = new Ray(transform.position + Vector3.up * 100, Vector3.down);
        if (Physics.Raycast(ray, out rayCastHit, Mathf.Infinity, layerMask))
        {
            transform.position = rayCastHit.point;
        }

        Vector3 lookAtPosition = Camera.main.transform.position;
        lookAtPosition.y = transform.position.y;
        transform.LookAt(lookAtPosition);
    } 

    protected override void Initialise()
    {

    }
}
