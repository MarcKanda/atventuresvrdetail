﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SelectionPointer : MonoBehaviour {

    public VRTK_ControllerEvents controllerEvents;
    public ToggleWorld world;

    public void Awake()
    {
        controllerEvents.GripClicked += GripClicked;
    }
    private void GripClicked(object sender, ControllerInteractionEventArgs e)
    {
        world.SetScale(false);
    }
}
