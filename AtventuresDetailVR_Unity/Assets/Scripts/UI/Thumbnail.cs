﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Thumbnail : MonoBehaviour {

    public Image image;

    private void Awake()
    {
        GetThumbnail();
    }

    public void Reset()
    {
        GetThumbnail();
    }

    private void GetThumbnail()
    {
        if (image == null)
            image = GetComponent<Image>();
    }
}
