﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VRTK;

public class InventoryMenu : MonoBehaviour {

    [Header("Item Group Setup ")]
    [SerializeField]
    ToggleGroup toggleGroup;

    [SerializeField]
    GameObject groupPrefab;

    [SerializeField]
    Transform groupParent;

    [SerializeField]
    InventoryGroup[] groups;

    [Header("Item Setup")]
    [SerializeField]
    ToggleGroup toggleItem;

    [SerializeField]
    GameObject itemPrefab;

    [SerializeField]
    Transform itemParent;


    [Header("Other")]

    [SerializeField]
    VRTK_InteractTouch touch;

    [SerializeField]
    VRTK_InteractGrab grab;

    [SerializeField]
    Transform spawnedItemParent;

    [SerializeField]
    VRTK_StraightPointerRenderer pointerRender;

    [SerializeField]
    VRTK_Pointer pointer;

    public void Start()
    {
        SetupGroupUI();
        
    }

    private void SetupGroupUI()
    {
        //We remove any existing groups (in case we have placed some UI for debugging purposes)
        groupParent.DestroyAllChildren();

        //We iterate over all the groups we have to spawn
        for (int i = 0; i < groups.Length; i++)
        {
            InventoryGroup group = groups[i];
            GameObject groupInstance = Instantiate(groupPrefab, groupParent, false);

            Thumbnail thumbnail = groupInstance.GetComponentInChildren<Thumbnail>();
            if(thumbnail != null)
            {
                thumbnail.image.sprite = group.groupThumbnail;
            }

            TextMeshProUGUI groupText = groupInstance.GetComponentInChildren<TextMeshProUGUI>();
            if (groupText != null)
            {
                groupText.text = group.groupName;
            }

            Toggle toggle = groupInstance.GetComponentInChildren<Toggle>();
            if(toggle != null)
            {
                toggle.interactable = !group.disableGroup;
                toggle.group = toggleGroup;
                
                //We assign a callback so we know which toggle have been clicked
                toggle.onValueChanged.AddListener(b => { SetupItemUI(group); });
            }

            if (i == 0)
                toggle.Select();
        }
    }

    private void SetupItemUI(InventoryGroup currentlySelectedGroup)
    {
        //We remove any existing groups (in case we have placed some UI for debugging purposes)
        itemParent.DestroyAllChildren();

        //We iterate over all the groups we have to spawn
        for (int i = 0; i < currentlySelectedGroup.items.Length; i++)
        {
            InventoryItem item = currentlySelectedGroup.items[i];

            GameObject itemInstance = Instantiate(itemPrefab, itemParent, false);

            Thumbnail thumbnail = itemInstance.GetComponentInChildren<Thumbnail>();
            if (thumbnail != null)
            {
                thumbnail.image.sprite = item.itemThumbnail;
            }

            TextMeshProUGUI temText = itemInstance.GetComponentInChildren<TextMeshProUGUI>();
            if (temText != null)
            {
                temText.text = item.itemName;
            }

            Toggle toggle = itemInstance.GetComponentInChildren<Toggle>();
            if (toggle != null)
            {
                toggle.group = toggleItem;
                //We assign a callback so we know which toggle have been clicked
                toggle.onValueChanged.AddListener(b => { ItemSelected(b, item); });
            }
        }
    }

    private void ItemSelected(bool toToggle, InventoryItem item)
    {
        if(toToggle)
        {
            GameObject itemInstance = Instantiate(item.itemPrefab, spawnedItemParent);
            VRTK_DestinationMarker m;

            touch.ForceTouch(itemInstance);
            grab.AttemptGrab();
        }
    }

    public void Update()
    {

    }
}
